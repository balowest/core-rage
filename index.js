const express = require ('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://Balowest:balowest@ds135790.mlab.com:35790/corerage');
//mongoose.connect('mongodb://localhost:27017/corerage');

var User = require('./checkdb');

const api = require('./api');

const app = express();

//enable CORS
// app.use(function(req, res, next){
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Acccept");
//   next();
// })

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

app.use('/api', api);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});





app.listen(process.env.PORT || 3000, function(){
    console.log('server is running on Balo port 3000!');
});

