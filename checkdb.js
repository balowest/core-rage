// var mongoose = require('mongoose');

// var Schema = mongoose.Schema;

// //this creates my own schema
// var checkSchema = new Schema({
//     name: String,
//     number: Number,
//     updated_at: Date,
//     created_at: Date

// });

// //this runs any time the save function is called
// checkSchema.pre('save', function(next){
//     var currentDate = new Date();
//     this.updated_at = currentDate;
//     if(!this.created_at){
//         this.created_at = currentDate;
//     }
//     next();
// })

// checkSchema.methods.about = function(){
//     let x = this.name + ' ' + this.number;
//     return x;
// }


// var ragerSchema = new Schema({
//     name: {type:String, required:true, unique:true, trim:true},
//     // surname: String,
//     // email: String,
//     // phone: String,
//     // password: String,
//     messages: [{ type: Schema.Types.ObjectId, ref: 'Message'}]
// });

// ragerSchema.pre('save', function(next){
//     Rager.find({"name":this.name}, "name", function(err, results){
//         let empty = [];
//         if (results[0]!=null){
//             return console.log(results);
//         }

//         next();
//     })
// })

// var messageSchema = new Schema({
//     rager_id: { type: String, ref: 'Rager' },
//     core_message: String,
//     time: String
// });

// var User = mongoose.model('User', checkSchema);
// var Rager = mongoose.model('Rager', ragerSchema);
// var Message = mongoose.model('Message', messageSchema);

// module.exports = {User, Rager, Message};