var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var groupSchema = new Schema({
    sender: {type: Schema.Types.ObjectId, ref:'User'},
    message: String,
    time: String
});

var Group = mongoose.model('Group', groupSchema);

module.exports = Group;