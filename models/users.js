var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: String,
    email: String,
    password: String,
    reg_time: String,
    update_time: String,
    messages: [{type: Schema.Types.ObjectId, ref:'Message'}]
});

userSchema.pre('save', function(next){
    User.find({"name":this.name}).exec(function(err, results){
        if (results[0]!=null){
            return console.log("This email is already in use");
           // res.send("Email already in use")
        }
        next();
    })
    
})

var User = mongoose.model('User', userSchema);

module.exports = User;