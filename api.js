const express = require('express');
const router = express.Router();
const User = require('./models/users');
const Message = require('./models/messages');
const Group = require('./models/groupchat');

//this function registers new users
router.post('/register', function (req, res) {
    var currentTime = new Date();
    var Rager = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        reg_time: currentTime,
        update_time: null
    });

    Rager.save(function (err) {
        if (err) res.send(err);
        //res.send();
    });
});

router.post('/login', function (req, res){
    User.find({"email":req.body.email, "password":req.body.password}).exec(function(err, result){
        if (err) throw err;
        res.send(result);
    })
})

//this function searches for all users
router.get('/', function (req, res) {
    User.find().sort({
        "_id": -1
    }).select({
        "name": 2,
        "email": 1
    }).exec(function (err, data) {
        if (err) throw err;
        res.send(data);
        console.log(data);
    })
})

//this sends a direct message
router.post('/sendmessage', function (req, res) {
    var currentTime = new Date();
    var Message1 = new Group({
        sender: req.body.sender,
        receiver: req.body.receiver,
        message: req.body.message,
        time: currentTime
    });

    Message1.save(function (err) {
        if (err) throw err;
        res.send("Message successfully saved");
        console.log("Nice one Balo")
    });
})

//this sends a general message
router.post('/groupchat', function (req, res) {
    var currentTime = new Date();
    var Group1 = new Group({
        sender: req.body.sender,
        message: req.body.message,
        time: currentTime
    })

    Group1.save(function (err) {
        if (err) throw err;
        res.send('{"resp":"Message Sent!"}');
    })
})

//this reads the general messages
router.get('/generalmessage', function (req, res) {
    Group.find().sort({
        "_id": -1
    }).populate("sender", "_id name").exec(function (err, result) {
        if (err) throw err;
        res.send(result);
        console.log(result);
    })
})

module.exports = router;